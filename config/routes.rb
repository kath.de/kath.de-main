# frozen_string_literal: true

Rails.application.routes.draw do
  get 'settings/reload'
  get 'archiv/:year-:month-:day', to: 'tagesreporte#show', as: 'tagesreport',
                                  constraints: { year: /\d{4}/, month: /\d{2}/, day: /\d{2}/ }
  get 'wochenrueckblick', to: 'wochenrueckblicke#index', as: 'wochenrueckblicke'
  get 'wochenrueckblick/:slug', to: 'wochenrueckblicke#show', as: 'wochenrueckblick'
  get 'kommentar', to: 'kommentare#index', as: 'kommentare'
  get 'kommentar/archiv/:page', to: 'kommentare#index', as: 'kommentare_paginate'
  get 'kommentar/:slug', to: 'kommentare#show', as: 'kommentar'
  get 'redaktion/autoren/:slug', to: 'authors#show', as: 'author'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'home#index'
  get 'partner/banner', to: 'home#bannerplaetze'

  get '*slug', to: 'pages#show', as: 'page', constraints: lambda { |req|
    req.path.exclude? 'rails/active_storage'
  }
end

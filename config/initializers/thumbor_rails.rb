# frozen_string_literal: true

ThumborRails.setup do |config|
  config.server_url = 'https://thumbor.rabanus.kath.de'
  config.security_key = ENV['THUMBOR_SECURITY_KEY'] || ''
end

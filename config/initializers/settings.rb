# frozen_string_literal: true

class Settings
  def self.reload_cache
    all.each do |record|
      singleton_class.define_method(record.slug) { record }
    end
  end

  reload_cache
end

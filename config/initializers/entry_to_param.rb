# frozen_string_literal: true

class Entry
  def to_param
    slug
  end
end

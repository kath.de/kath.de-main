document.querySelectorAll("a[href^=\"http://\"], a[href^=\"https://\"]").forEach(element => {
  element.addEventListener("click", event => {
    var url = element.getAttribute("href");
    if(window.gtag) {
      gtag('event', 'click', {
        'event_category': 'outbound',
        'event_label': url,
        'transport_type': 'beacon',
        'event_callback': function(){document.location = url;}
      });
    }
    return false;
  });
});

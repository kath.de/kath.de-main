# frozen_string_literal: true

module PartnerinfoHelper
  def partnerinfo(type, id = nil)
    type = type.parameterize
    id = id.parameterize

    banners = Settings.werbebanner.dig(type, id)

    werbebanner_preview = @current_page.werbebanner_preview
    return unless banners.present? || werbebanner_preview

    class_prefix = 'partnerinfo'
    classes = [class_prefix, [class_prefix, type].join('--')]
    classes << [class_prefix, type, id].join('--') if id
    tag.div(class: classes.join(' ')) do
      if werbebanner_preview
        idinfo = [type, id].compact.join('.')
        ['Werbebanner', tag.code(idinfo)].join.html_safe
      else
        render partial: 'application/banner', collection: banners
      end
    end
  end
end

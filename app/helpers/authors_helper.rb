# frozen_string_literal: true

module AuthorsHelper
  def author_link(author, options = {})
    unless author.is_a? Author
      author = author.author if author.respond_to? :author
    end

    options = { rel: 'author' }.merge(options)

    link_to author.display_name, author_path(author), options
  end

  def author_links(authors, options = {})
    authors = authors.authors unless authors.respond_to? :map

    authors.map { |author| author_link(author, options) }.to_sentence.html_safe
  end
end

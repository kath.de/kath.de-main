# frozen_string_literal: true

module IconsHelper
  def social_icon(name)
    %(<svg class="social-icon"><use class="icon" xlink:href="#icon-#{name}" /></svg>).html_safe
  end
end

# frozen_string_literal: true

module ApplicationHelper
  def resized_image(image_path, size = [160, 120])
    # image = Image.by_slug(image_path)
    # image.variant(resize_to_limit: size)
  end

  def cover_image(entry, width = 750, height = nil, href: nil)
    cover = entry.cover || return

    height ||= width / 3

    image(cover, width, height, href: href)
  end

  def image(image, width, height, image_classes = [], href: nil)
    url = case Rails.configuration.image_resize_backend
          when :thumbor
            thumbor_url(rails_blob_url(image.image), width: width, height: height)
          else
            image.image.variant(resize_to_fill: [width, height])
          end

    image_classes = ['image'] + image_classes
    tag.div(class: image_classes) do
      output = image_tag(url, itemprop: 'image')

      if href
        output = link_to(output, href)
      end

      cite = image.cite
      unless cite == 'kath.de'
        output += tag.cite do
          markdown(cite)
        end
      end

      output
    end
  end

  def author_cover_image(author, size = 150)
    image = author.cover || return
    link_to author_path(author), { rel: 'author', class: 'author-cover-image' } do
      image(image, size, size)
    end
  end
end

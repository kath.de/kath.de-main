# frozen_string_literal: true

module TimeHelper
  def format_date(date, format = :long)
    date ||= Time.zone.now

    formats = {
      long: '%A, %-d. %B %Y',
      medium: '%-d. %B %Y',
      short: '%-d. %-m. %Y',
      day_month: '%-d. %B',
    }
    format = formats[format] if formats.key? format
    unless date.respond_to? :strftime
      date = date.gsub('–', '-')
      date = Date.parse(date)
    end

    I18n.localize(date, format: format)
  end

  def time_tag(date_or_time, *args, &block)
    options  = args.extract_options!
    format   = options.delete(:format) || :long
    content  = args.first || format_date(date_or_time, format)

    tag.time(content, options.reverse_merge(datetime: date_or_time.iso8601), &block)
  end
end

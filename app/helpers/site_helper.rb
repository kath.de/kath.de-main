# frozen_string_literal: true

module SiteHelper
  def self.markdown(source)
    return unless source

    Tilt['markdown'].new { source }.render.html_safe
  end

  def markdown(source)
    SiteHelper.markdown(source)
  end

  def split_lead_paragraph(markdown)
    parts = markdown.split(/\n\n/, 2)
    parts = [''] + parts if parts.size == 1
    parts
  end

  def wrap_title(title, suffix = nil, separator = '|')
    [
      strip_tags(title),
      ' ', separator, ' ',
      strip_tags(suffix || Settings.site.title),
    ].join.strip
  end

  def current_page
    @current_page ||= PageContext::Base.new
  end

  def current_page=(page)
    @current_page = page
  end

  def toc(source, &block)
    doc = Kramdown::Document.new(source)
    toc = Kramdown::Converter::Toc.convert(doc.root)

    toc.first.children.each do |child|
      render_toc(child, &block)
    end
  end

  def render_toc(element, &block)
    block.call element.value.children.first.value, element.attr[:id]

    element.children.each do |child|
      render_toc(child, &block)
    end
  end
end

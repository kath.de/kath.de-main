# frozen_string_literal: true

class PagesController < ApplicationController
  def show
    @page = Page.by_slug(params[:slug]).first

    unless @page
      head :not_found
      return
    end

    @current_page = PageContext::Page.new @page

    render
  end
end

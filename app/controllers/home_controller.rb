# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @tagesreport = Tagesreport.newest

    @current_page = PageContext::Tagesreport.new @tagesreport

    render
  end

  def bannerplaetze
    @tagesreport = Tagesreport.newest

    @current_page = PageContext::Tagesreport.new @tagesreport

    @current_page.werbebanner_preview = true

    @page_content = render_to_string('home/index', layout: false)

    render
  end
end

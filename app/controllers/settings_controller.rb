# frozen_string_literal: true

class SettingsController < ApplicationController
  def reload
    Settings.reload_cache

    head :ok
  end
end

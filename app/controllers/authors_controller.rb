# frozen_string_literal: true

class AuthorsController < ApplicationController
  def show
    @author = Author.by_slug(params[:slug]).first
    unless @author
      head :not_found
      return
    end

    @current_page = PageContext::Author.new @author

    @recent_works = @author.recent_kommentare

    render
  end
end

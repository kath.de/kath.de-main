# frozen_string_literal: true

class WochenrueckblickeController < ApplicationController
  def index
    @wochenrueckblicke = Wochenrueckblick.recent.limit(5)

    @current_page = PageContext::Collection.new

    render
  end

  def show
    preview_token = params[:preview_token]
    relation = Wochenrueckblick.published
    if preview_token
      relation = relation.or(Wochenrueckblick.drafts_with_token(preview_token))
    end

    @wochenrueckblick = relation.by_slug(params[:slug]).first

    unless @wochenrueckblick
      head :not_found
      return
    end

    published_at = @wochenrueckblick.published_at || @wochenrueckblick.updated_at
    @aktueller_kommentar = Kommentar.recent.where('published_at < ?', published_at + 1.day).first

    @current_page = PageContext::Wochenrueckblick.new @wochenrueckblick

    render
  end
end

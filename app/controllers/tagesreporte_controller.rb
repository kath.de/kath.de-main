# frozen_string_literal: true

class TagesreporteController < ApplicationController
  def show
    preview_token = params[:preview_token]
    relation = Tagesreport.published
    if preview_token
      relation = relation.or(Tagesreport.drafts_with_token(preview_token))
    end

    @tagesreport = relation.for_date(params[:year], params[:month], params[:day]).first

    unless @tagesreport
      head :not_found
      return
    end

    @current_page = PageContext::Tagesreport.new @tagesreport

    render
  end
end

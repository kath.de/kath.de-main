# frozen_string_literal: true

class KommentareController < ApplicationController
  def show
    preview_token = params[:preview_token]
    relation = Kommentar.published
    if preview_token
      relation = relation.or(Kommentar.drafts_with_token(preview_token))
    end

    @kommentar = relation.by_slug(params[:slug]).first

    unless @kommentar
      head :not_found
      return
    end

    @current_page = PageContext::Kommentar.new @kommentar

    render
  end

  def index
    @kommentare = Kommentar.recent.page(params["page"]).per(10).without_count

    @current_page = PageContext::Collection.new(@kommentare)

    render
  end
end

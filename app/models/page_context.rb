# frozen_string_literal: true

module PageContext
  class Base
    include ThumborRails::Helpers

    attr_accessor :relation, :metadata, :amp, :werbebanner_preview,
      :html_meta

    def initialize(relation = nil, metadata = {})
      @relation = relation
      @metadata = metadata
      @amp = false
      @werbebanner_preview = false
      @html_meta = {}

      initialize_values
    end

    def initialize_values
      if relation.respond_to?(:cover) && (cover = relation.cover)
        html_meta["og:image"] = thumbor_url(Rails.application.routes.url_helpers.rails_blob_url(cover.image), width: 1200, height: 630)
      end
      if title
        html_meta["og:title"] = title
      end
      if description
        html_meta["og:description"] = description
      end
    end

    def authors
      metadata.fetch(:authors, [])
    end

    def title
      metadata.fetch(:title, nil)
    end

    def description
      metadata.fetch(:description, nil)
    end

    def canonical_url
      nil
    end

    def amp?
      amp
    end

    def preview?
      false
    end
  end

  class Single < Base
    def authors
      relation.try(:authors) || super
    end

    def title
      relation.try(:title) || super
    end

    def preview?
      relation.unpublished? && relation.respond_to?(:preview_token)
    end
  end

  class Collection < Base
    attr_accessor :page_body
  end

  class Tagesreport < Single
    def aktueller_kommentar
      ::Kommentar.recent.first
    end
  end

  class Author < Single
    def body
      body = @relation.body
      body = @relation.short_bio if body.nil? || body == ''
      body
    end

    def title
      relation.display_name
    end
  end

  class Kommentar < Single
    def description
      SiteHelper.markdown(relation.teaser_text)
    end
  end

  class Wochenrueckblick < Single
    def description
      SiteHelper.markdown(relation.teaser_text)
    end
  end

  class Page < Single
    def description
      SiteHelper.markdown(relation.body.truncate(120))
    end

    def title
      relation.long_title || relation.title
    end
  end
end

# frozen_string_literal: true

module TemplateLocals
  def assign_locals(locals)
    @_default_locals = _default_locals.merge locals
  end

  private

  def _default_locals
    @_default_locals || {}
  end

  def _default_render_options
    super.merge locals: _default_locals
  end
end

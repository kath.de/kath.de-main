# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/kommentar' do
  let(:specific_comment) { Kommentar.recent.first }

  it 'displays title' do
    visit '/kommentar'
    expect(page.status_code).to match(200)

    expect(page.title.chomp).to eq('kath.de-Kommentar')
  end

  it 'navigates to kommentar page' do
    visit '/kommentar'

    click_on specific_comment.title

    expect(page.status_code).to match(200)
    expect(page.title.chomp).to match specific_comment.title
  end
end

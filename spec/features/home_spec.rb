# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Homes', type: :feature do
  it 'User visits home page' do
    visit '/'
    expect(page.status_code).to match(200)

    expect(page).to have_content('kath.de')
    expect(page.title.chomp).to eq('kath.de | Unabhängige katholische Nachrichten')

    expect(page).to have_content('Bischofskonferenz in Passau')
  end

  it 'Users visits Bannerplätze' do
    visit('/partner/banner')

    expect(page.title).to eq 'Werbebanner-Platzierung | kath.de'
    expect(page).to have_content 'Werbebannernews-section.themen-des-tages'
  end

  it 'User registers for newsletter' do
    visit '/'

    expect(page).to have_content('Den Wochenrückblick von kath.de jeden Freitag kostenlos als Newsletter per E-Mail erhalten.')

    fill_in 'E-Mail', with: 'test@kath.de'
    expect(page).to have_selector(:link_or_button, 'Anmelden')
  end

  it 'shows social links' do
    visit '/'

    expect(page).to have_content('Twitter')
    expect(page).to have_content('Facebook')
  end
end

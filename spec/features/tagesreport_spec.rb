# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'tagesreport' do
  it 'displays aufmacher' do
    visit '/archiv/2015-12-29'
    expect(page.status_code).to match(200)

    expect(page).to have_content('kath.de')
    expect(page.title.chomp).to eq('Nachrichtenüberblick vom Dienstag, 29. Dezember 2015 | kath.de')

    expect(page).to have_content('Meer aus Königen: Sternsinger brechen auf')
  end

  context 'draft' do
    it 'hides page' do
      visit '/archiv/2017-02-07'

      expect(page.status_code).to match(404)
    end

    it 'is visible with preview_token' do
      preview_token = Tagesreport.by_slug('2017-02-07').first.preview_token

      visit "/archiv/2017-02-07?preview_token=#{preview_token}"

      expect(page.status_code).to match(200)
      expect(page).to have_content('Keine Hilfe vom Vatikan: Ermittler beklagen mangelnde Kooperation')
      expect(page).to have_content('Entwurfsvorschau')
    end
  end

  context 'published' do
    it 'is visible despite preview_token' do
      visit "/archiv/2015-12-29?preview_token=foo bar"

      expect(page.status_code).to match(200)
      expect(page.title.chomp).to eq('Nachrichtenüberblick vom Dienstag, 29. Dezember 2015 | kath.de')

      expect(page).to have_content('Meer aus Königen: Sternsinger brechen auf')
    end
  end
end

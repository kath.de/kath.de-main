# frozen_string_literal: true

require 'spec_helper'

RSpec.describe '/wochenrueckblick' do
  let(:specific_entry) { Wochenrueckblick.recent.first }

  it 'displays title' do
    visit '/wochenrueckblick'
    expect(page.status_code).to match(200)

    expect(page.title.chomp).to eq('kath.de Die Woche')
  end

  it 'navigates to wochenrueckblick page' do
    visit '/wochenrueckblick'

    click_on specific_entry.title

    expect(page.status_code).to match(200)
    expect(page.title.chomp).to match specific_entry.title
  end
end

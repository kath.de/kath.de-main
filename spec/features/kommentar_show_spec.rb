# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/kommentar/:slug' do
  context 'invalid slug' do
    it 'shows 404' do
      visit '/kommentar/__der-nicht-existiert'

      expect(page.status_code).to match(404)
    end
  end

  it 'displays title' do
    visit '/kommentar/2016-01-08-recht-gegen-angst'
    expect(page.status_code).to match(200)

    expect(page.title).to eq('Recht gegen Angst | kath.de-Kommentar')
  end

  context 'draft kommentar' do
    it 'hides page' do
      visit '/kommentar/2017-02-09-entwurfskommentar'

      expect(page.status_code).to match(404)
    end

    it 'is visible with preview_link' do
      preview_token = Kommentar.by_slug('2017-02-09-entwurfskommentar').first.preview_token

      visit "/kommentar/2017-02-09-entwurfskommentar?preview_token=#{preview_token}"

      expect(page.status_code).to match(200)
      expect(page).to have_content('Entwurfsvorschau')
    end
  end
end

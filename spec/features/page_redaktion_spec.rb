# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/redaktion' do
  it 'displays title' do
    visit '/redaktion'
    expect(page.status_code).to match(200)

    expect(page.title.chomp).to eq('Die kath.de-Redaktion | kath.de')
    expect(page).to have_content('Das ist die Redaktion von kath.de:')
  end

  it 'non-existing page' do
    visit '/page-does_not_exist'
    expect(page.status_code).to match(404)
  end
end

# frozen_string_literal: true

require 'spec_helper'

RSpec.describe '/wochenrueckblick/:slug' do
  context 'invalid slug' do
    it 'shows 404' do
      visit '/wochenrueckblick/__der-nicht-existiert'

      expect(page.status_code).to match(404)
    end
  end

  it 'displays title' do
    visit '/wochenrueckblick/2018-06-08'
    expect(page.status_code).to match(200)

    expect(page.title).to eq('Die Woche vom 2. bis 8. Juni 2018 | kath.de – Die Woche')
    expect(page).to have_content('kath.de – Die Woche')
  end

  it 'displays kommentar' do
    visit '/wochenrueckblick/2018-06-08'
    expect(page.status_code).to match(200)

    expect(page).to have_content('kath.de-Wochenkommentar')
    expect(page).to have_content('„Nach der Vorabendmesse bist du fällig“')
    expect(page).to have_content('Der Fall des zurückgetreten Pfarrers Olivier Ndjimbi-Tshiende')
    expect(page).to have_link('Lesen Sie den vollständigen Kommentar hier')
  end

  it 'displays newsletter signup' do
    visit '/wochenrueckblick/2018-06-08'

    expect(page).to have_content('Anmeldung zum Newsletter')
  end

  context 'draft wochenrueckblick' do
    it 'hides page' do
      visit '/wochenrueckblick/2018-06-15'

      expect(page.status_code).to match(404)
    end

    it 'is visible with preview_link' do
      preview_token = Wochenrueckblick.by_slug('2018-06-15').first.preview_token

      visit "/wochenrueckblick/2018-06-15?preview_token=#{preview_token}"

      expect(page.status_code).to match(200)
      expect(page).to have_content('Entwurfsvorschau')
    end
  end
end

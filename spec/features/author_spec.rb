# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/redaktion/autoren/:slug' do
  context 'invalid slug' do
    it 'shows 404' do
      visit '/redaktion/autoren/__der-nicht-existiert'

      expect(page.status_code).to match(404)
    end
  end

  it 'displays title' do
    visit '/redaktion/autoren/philipp-mueller'
    expect(page.status_code).to match(200)

    expect(page.title).to eq('Philipp Mueller | kath.de')
    expect(page).to have_content('Bei kath.de schreibe ich Texte.')
  end
end

require "rails_helper"
require 'kathde/models'
require 'capybara/rspec'

RSpec.describe 'news/_entry.html.erb', type: :view do
  describe "date" do
    it 'shows default date for given datum' do
      assign_locals(entry: Tagesreport::ReportEntry.new({
        "titel" => "Testeintrag",
        "datum" => Time.new(2018, 12, 25)
        }))

      @current_page = PageContext::Tagesreport.new

      render

      expect(rendered).to include('<span class="date">25. Dezember 2018</span>')
    end

    it 'hides date for empty datum' do
      assign_locals(entry: Tagesreport::ReportEntry.new({
        "titel" => "Testeintrag",
        "datum" => false
        }))

      @current_page = PageContext::Tagesreport.new

      render

      expect(rendered).to_not include('class="date"')
    end
  end

  it 'renders related entries' do
    entry = Tagesreport::ReportEntry.new({
      "titel" => "Testeintrag",
      "datum" => Time.new(2018, 12, 25),
      })
    entry.related = [Tagesreport::ReportEntry.new({
      "titel" => "Testeintrag2",
      "datum" => Time.new(2018, 12, 26),
      })]
    assign_locals(entry: entry)

    @current_page = PageContext::Tagesreport.new

    render

    expect(rendered).to include('<span class="date">26. Dezember 2018</span>')
    expect(rendered).to include('Testeintrag2')
  end
end

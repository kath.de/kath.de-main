# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SettingsController do
  # rubocop:disable RSpec/ExampleLength
  it 'reloads' do
    site = Settings['site']
    site.settings['test_setting'] = 'Foo'
    site.save

    expect(Settings.site.settings['test_setting']).to be_nil

    get 'reload'

    expect(Settings.site.settings['test_setting']).to eq 'Foo'
  ensure
    site = Settings['site']
    site.settings.delete('test_setting')
    site.save

    Settings.reload_cache
  end
  # rubocop:enable RSpec/ExampleLength
end

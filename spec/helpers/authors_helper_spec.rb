# frozen_string_literal: true

require 'spec_helper'

RSpec.describe AuthorsHelper, type: :helper do
  describe 'author_link' do
    before do
      allow(helper).to receive(:author_path) { |author| "/redaktion/autoren/#{author.slug}" }
    end

    let(:author) { Author.new(display_name: 'Alexander Radej', slug: 'alexander-radej') }
    let(:author_html) { '<a rel="author" href="/redaktion/autoren/alexander-radej">Alexander Radej</a>' }

    context 'with a single author' do
      it 'returns author link for author object' do
        expect(helper.author_link(author)).to match(author_html)
      end

      it 'is html_safe' do
        expect(helper.author_link(author)).to be_html_safe
      end

      it 'returns author link for entry object' do
        dummy_entry = instance_double('Entry', author: author)
        expect(helper.author_link(dummy_entry)).to match(author_html)
      end
    end

    context 'with multiple authors' do
      let(:authors) do
        [
          author,
          Author.new(display_name: 'Maximilian C. Röll', slug: 'maximilian-roell'),
          Author.new(display_name: 'Philipp Müller', slug: 'philipp-mueller'),
        ]
      end
      let(:authors_html) { %(#{author_html}, <a rel="author" href="/redaktion/autoren/maximilian-roell">Maximilian C. Röll</a> und <a rel="author" href="/redaktion/autoren/philipp-mueller">Philipp Müller</a>) }

      it 'returns author link for author object' do
        expect(helper.author_links(authors)).to match(authors_html)
      end

      it 'is html_safe' do
        expect(helper.author_links(authors)).to be_html_safe
      end

      it 'returns author link for entry object' do
        dummy_entry = instance_double('Entry', authors: authors)
        expect(helper.author_links(dummy_entry)).to match(authors_html)
      end
    end
  end
end
